# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 文件说明
from Commons.ui_auto.basic_page import BasicPage
from PageObject import elements

import allure


class PageBaidu(BasicPage):
    # 实例化的百度定位元素
    ele_baidu = elements.PageBaidu()

    @allure.step("访问百度页面")
    def get_baidu(self):
        self.get_url(url=self.ele_baidu.url)

    @allure.step("输入需要查询的关键字")
    def input_keyword(self, keyword):
        self.input_text(locator=self.ele_baidu.input, content=keyword)

    @allure.step("点击百度一下")
    def click_baidu_button(self):
        self.click_element(locator=self.ele_baidu.baiduButton)
