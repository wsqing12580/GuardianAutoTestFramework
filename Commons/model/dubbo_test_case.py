# -*- coding: utf-8 -*-
# -------------------------------
# @文件：dubbo_test_case.py
# @时间：2024/1/25 14:45
# @作者：caiweichao
# @功能描述：dubbo接口用例模型
# -------------------------------


class DubboTestCase:
    service = None
    service_name = None
    method_name = None
    title = None
    casedata = None
    jsonpath = None
    result = None
    user_name = None
    password = None
    model = None
    more = None
    load = None
