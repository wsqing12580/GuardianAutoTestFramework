# -*- coding: utf-8 -*-
# -------------------------------
# @文件：api_test_case.py
# @时间：2024/1/25 14:44
# @作者：caiweichao
# @功能描述：接口用例模型
# -------------------------------


class ApiTestCase:
    case_id: str = None
    case_title: str = None
    url: str = None
    data: dict = None
    method: str = None
    check: str = None
    jsonpath: str = None
    env: str = None
    sql: str = None
    sql_check: str = None
