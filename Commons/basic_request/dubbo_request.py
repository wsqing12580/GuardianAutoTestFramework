# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 请求dubbo的工具类 (未实现)


# class DubboRequest:
#     def req_dubbo(self, url, interface, method, param_obj, **kwargs):
#         """
#         :param url: url地址
#         :param interface: 接口名称，因为这里可能还有别的服务要测，接口名不一样，这里定义成变量
#         :param method: 调用哪个方法
#         :param param_obj: 入参的对象
#         :param kwargs: 这个用关键字参数，因为每个接口的参数都不一样，不固定，所以这里用关键字参数
#         """
#         # 这个是用来构造二进制的入参的，也就是把入参序列化
#         res_param = protocol.object_factory(param_obj, **kwargs)
#         try:
#             # 这个res是生成一个请求对象
#             req_obj = HessianProxy(url + interface)
#             # getattr是python的内置方法，获取对象的方法，咱们从构造的请求对象里面获取到方法，
#             # 然后调用，把前面生成的 序列化好的参数传进去，然后获取到返回的数据
#             res = getattr(req_obj, method)(res_param)
#             return res
#         except Exception as e:
#             Log.error(f"请求出现异常，异常信息是：\n{e}")
#
#
# if __name__ == '__main__':
#     url = 'http://192.168.1.225:21884/'
#     interface = 'com.tem.platform.api.PartnerService'
#     method = 'findById'
#     param_obj = 'com.tem.platform.api.param.Param'
#     params = {"id": 192}
#     value = DubboRequest().req_dubbo(url, interface, method, param_obj, **params)
#     print(value)
