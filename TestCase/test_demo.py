# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : ui自动化demo
import time

from PageObject.page_baidu import PageBaidu

import pytest
import allure


# 使用打开浏览器的测试夹具（会自动识别chrome浏览器版本并且安装对应的driver）
@pytest.mark.usefixtures("global_step")
@pytest.mark.usefixtures("kill_driver")
@allure.feature('访问百度demo')
class TestDemo:
    def test_baidu_demo1(self, global_step):
        PageBaidu(global_step).get_baidu()
        PageBaidu(global_step).input_keyword(keyword="gitee")
        PageBaidu(global_step).click_baidu_button()
        time.sleep(3)

    def test_baidu_demo2(self, global_step):
        PageBaidu(global_step).get_baidu()
        PageBaidu(global_step).input_keyword(keyword="github")
        PageBaidu(global_step).click_baidu_button()
        time.sleep(3)
